window:
  opacity: 0.85
  # opacity: 1.0

  padding:
    x: 5
    y: 5
  decorations: none
  startup_mode: Maximized

font:
  normal:
    # family: "Fira Code"
    # family: Inconsolata-g
    # JetBrainsMono works with icons and ligatures
    family: "JetBrainsMono NF"
  size: 11
  offset:
    x: 0
    y: 2
  glyph_offset:
    x: 0
    y: 1

cursor:
  # Cursor style
  style:
    # Cursor shape
    #
    # Values for `shape`:
    #   - ▇ Block
    #   - _ Underline
    #   - | Beam
    shape: Beam

    # Cursor blinking state
    #
    # Values for `blinking`:
    #   - Never: Prevent the cursor from ever blinking
    #   - Off: Disable blinking by default
    #   - On: Enable blinking by default
    #   - Always: Force the cursor to always blink
    blinking: Off

  # Vi mode cursor style
  #
  # If the vi mode cursor style is `None` or not specified, it will fall back to
  # the style of the active value of the normal cursor.
  #
  # See `cursor.style` for available options.
  #vi_mode_style: None

  # Cursor blinking interval in milliseconds.
  blink_interval: 750

  # If this is `true`, the cursor will be rendered as a hollow box when the
  # window is not focused.
  unfocused_hollow: true

  # Thickness of the cursor relative to the cell width as floating point number
  # from `0.0` to `1.0`.
  thickness: 0.15

draw_bold_text_with_bright_colors: false

schemes:

# Colors (Nord)
  nord2: &nord-bright
    primary:
        background: '#f6f8fa'
        foreground: '#2e3440'
        dim_foreground: '#d8dee9'
    cursor:
        text: '#2e3440'
        cursor: '#d8dee9'
    vi_mode_cursor:
        text: '#2e3440'
        cursor: '#d8dee9'
    selection:
        text: CellForeground
        background: '#4c566a'
    search:
        matches:
            foreground: CellBackground
            background: '#88c0d0'
        bar:
            background: '#434c5e'
            foreground: '#d8dee9'
    normal:
        black: '#3b4252'
        red: '#bf616a'
        green: '#a3be8c'
        yellow: '#ebcb8b'
        blue: '#81a1c1'
        magenta: '#b48ead'
        cyan: '#88c0d0'
        white: '#e5e9f0'
    bright:
        black: '#4c566a'
        red: '#bf616a'
        green: '#a3be8c'
        yellow: '#ebcb8b'
        blue: '#81a1c1'
        magenta: '#b48ead'
        cyan: '#8fbcbb'
        white: '#eceff4'
    dim:
        black: '#373e4d'
        red: '#94545d'
        green: '#809575'
        yellow: '#b29e75'
        blue: '#68809a'
        magenta: '#8c738c'
        cyan: '#6d96a5'
        white: '#aeb3bb'

# Colors (Nord)
  nord: &nord-dark
    # Default colors
    primary:
      # background: '#2E3440'
      background: '#000000'
      foreground: '#D8DEE9'


    # Normal colors
    normal:
      black:   '#3B4252'
      red:     '#BF616A'
      green:   '#A3BE8C'
      yellow:  '#EBCB8B'
      blue:    '#81A1C1'
      magenta: '#B48EAD'
      cyan:    '#88C0D0'
      white:   '#E5E9F0'

    # Bright colors
    bright:
      black:   '#4C566A'
      red:     '#BF616A'
      green:   '#A3BE8C'
      yellow:  '#EBCB8B'
      blue:    '#81A1C1'
      magenta: '#B48EAD'
      cyan:    '#8FBCBB'
      white:   '#ECEFF4'

  jellybean: &jellybean
    # Default colors
    primary:
      background: '#161616'
      foreground: '#e4e4e4'

    # Cursor volors
    cursor:
      text: '#feffff'
      cursor: '#ffb472'

    # Normal colors
    normal:
      black:   '#a3a3a3'
      red:     '#e98885'
      green:   '#a3c38b'
      yellow:  '#ffc68d'
      blue:    '#a6cae2'
      magenta: '#e7cdfb'
      cyan:    '#00a69f'
      white:   '#e4e4e4'

    # Bright colors
    bright:
      black:   '#c8c8c8'
      red:     '#ffb2b0'
      green:   '#c8e2b9'
      yellow:  '#ffe1af'
      blue:    '#bddff7'
      magenta: '#fce2ff'
      cyan:    '#0bbdb6'
      white:   '#feffff'

    # Selection colors
    selection:
      text: '#5963a2'
      background: '#f6f6f6'

  atelier-dune: &atelier-dune
    # Default colors
    primary:
      background: '0x20201d'
      foreground: '0xa6a28c'

    # Colors the cursor will use if `custom_cursor_colors` is true
    cursor:
      text: '0x20201d'
      cursor: '0xa6a28c'

    # Normal colors
    normal:
      black:   '0x20201d'
      red:     '0xd73737'
      green:   '0x60ac39'
      yellow:  '0xae9513'
      blue:    '0x6684e1'
      magenta: '0xb854d4'
      cyan:    '0x1fad83'
      white:   '0xa6a28c'

    # Bright colors
    bright:
      black:   '0x7d7a68'
      red:     '0xd73737'
      green:   '0x60ac39'
      yellow:  '0xae9513'
      blue:    '0x6684e1'
      magenta: '0xb854d4'
      cyan:    '0x1fad83'
      white:   '0xfefbec'

    indexed_colors:
      - { index: 16, color: '0xb65611' }
      - { index: 17, color: '0xd43552' }
      - { index: 18, color: '0x292824' }
      - { index: 19, color: '0x6e6b5e' }
      - { index: 20, color: '0x999580' }
      - { index: 21, color: '0xe8e4cf' }

  # Colors (Gruvbox light)
  gruvbox-light: &gruvbox-light
    # Default colors
    primary:
      # hard contrast: background = '#f9f5d7'
      background: '#fbf1c7'
      # soft contrast: background = '#f2e5bc'
      foreground: '#3c3836'

    # Normal colors
    normal:
      black:   '#fbf1c7'
      red:     '#cc241d'
      green:   '#98971a'
      yellow:  '#d79921'
      blue:    '#458588'
      magenta: '#b16286'
      cyan:    '#689d6a'
      white:   '#7c6f64'

    # Bright colors
    bright:
      black:   '#928374'
      red:     '#9d0006'
      green:   '#79740e'
      yellow:  '#b57614'
      blue:    '#076678'
      magenta: '#8f3f71'
      cyan:    '#427b58'
      white:   '#3c3836'

  solarized-light: &solarized-light
    # Default colors
    primary:
        background: '0xfdf6e3'
        foreground: '0x586e75'

    # Normal colors
    normal:
        black:   '0x073642'
        red:     '0xdc322f'
        green:   '0x859900'
        yellow:  '0xb58900'
        blue:    '0x268bd2'
        magenta: '0xd33682'
        cyan:    '0x2aa198'
        white:   '0xeee8d5'

    # Bright colors
    bright:
        black:   '0x002b36'
        red:     '0xcb4b16'
        green:   '0x586e75'
        yellow:  '0x657b83'
        blue:    '0x839496'
        magenta: '0x6c71c4'
        cyan:    '0x93a1a1'
        white:   '0xfdf6e3'

# Colors (Pencil Light)
  pencil-light: &pencil-light
    # Default Colors
    primary:
        background: '0xffffff'
        foreground: '0x424242'
    # Normal colors
    normal:
        black:   '0x212121'
        red:     '0xc30771'
        green:   '0x10a778'
        yellow:  '0xa89c14'
        blue:    '0x008ec4'
        magenta: '0x523c79'
        cyan:    '0x20a5ba'
        white:   '0xe0e0e0'
    # Bright colors
    bright:
        black:   '0x212121'
        red:     '0xfb007a'
        green:   '0x5fd7af'
        yellow:  '0xf3e430'
        blue:    '0x20bbfc'
        magenta: '0x6855de'
        cyan:    '0x4fb8cc'
        white:   '0xf1f1f1'

  gruvbox-dark: &gruvbox-dark
    # Default colors
    primary:
      background: '0x1d2021'
      foreground: '0xd5c4a1'

    # Colors the cursor will use if `custom_cursor_colors` is true
    cursor:
      text: '0x1d2021'
      cursor: '0xd5c4a1'

    # Normal colors
    normal:
      black:   '0x1d2021'
      red:     '0xfb4934'
      green:   '0xb8bb26'
      yellow:  '0xfabd2f'
      blue:    '0x83a598'
      magenta: '0xd3869b'
      cyan:    '0x8ec07c'
      white:   '0xd5c4a1'

    # Bright colors
    bright:
      black:   '0x665c54'
      red:     '0xfb4934'
      green:   '0xb8bb26'
      yellow:  '0xfabd2f'
      blue:    '0x83a598'
      magenta: '0xd3869b'
      cyan:    '0x8ec07c'
      white:   '0xfbf1c7'

    indexed_colors:
      - { index: 16, color: '0xfe8019' }
      - { index: 17, color: '0xd65d0e' }
      - { index: 18, color: '0x3c3836' }
      - { index: 19, color: '0x504945' }
      - { index: 20, color: '0xbdae93' }
      - { index: 21, color: '0xebdbb2' }

  ayu-light: &ayu-light
    primary:
      background: '0xfafafa'
      foreground: '0x5b6672'

    # Normal colors
    normal:
      black: '0x000000' #0
      red: '0xf2590b' #1
      green: '0x76cc00' #2
      yellow: '0xf29717' #3
      blue: '0x41a5d9' #4
      magenta: '0x9965cc' #5
      cyan: '0x4dbf98' #6
      white: '0xc7c7c7' #7

    # Bright colors
    bright:
      black: '0x676767'
      red: '0xd6646a' #9
      green: '0xa3d900' #10
      yellow: '0xe7c446' #11
      blue: '0x6871ff' #12
      magenta: '0xa37acc' #13
      cyan: '0x56d9ad' #14
      white: '0xfeffff' #15

# colors: *nord-dark
colors: *gruvbox-dark
