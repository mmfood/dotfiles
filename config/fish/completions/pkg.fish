function __fish_pkg_complete_list_pkgs
    ls -1 ~/.local/src
end
function __fish_pkg_complete_find_aur_pkgs
    set -l cmd (commandline -pot)
    set -l query $cmd[1]
    echo "args: " $cmd[1]  > args.txt
    curl "https://aur.archlinux.org/packages?O=0&K=$query" 2> /dev/null \
        | grep "href=\"/packages/" | tr -d '<>"' | cut -d/ -f3
end

set -l pkg_commands list install remove update
set -l forcable_commands remove update

# Don't list files in cwd #
complete -c pkg -f

# Possible commands
complete -c pkg -f -n "not __fish_seen_subcommand_from $pkg_commands" -a list -d 'List all AUR packages'
complete -c pkg -f -n "not __fish_seen_subcommand_from $pkg_commands" -a install -d 'Install a new AUR package'
complete -c pkg -f -n "not __fish_seen_subcommand_from $pkg_commands" -a remove -d 'Remove an AUR package'
complete -c pkg -f -n "not __fish_seen_subcommand_from $pkg_commands" -a update -d 'Update an existing AUR package'

# Add -f option
complete -c pkg -f -n "not __fish_contains_opt -s f; and not __fish_seen_subcommand_from list install" -a "-f" -d 'Force action'

# complete with package names if command is given with the -f flag
complete -c pkg -f -n "__fish_seen_subcommand_from $forcable_commands" -s f -x -a "(__fish_pkg_complete_list_pkgs)"
# otherwise complete with a command
complete -c pkg -f -n "not __fish_seen_subcommand_from $forcable_commands" -s f -x -a "$forcable_commands"

# Complete with list of packages for remove and update
complete -c pkg -f -n "__fish_seen_subcommand_from remove" -a "(__fish_pkg_complete_list_pkgs)"
complete -c pkg -f -n "__fish_seen_subcommand_from update" -a "(__fish_pkg_complete_list_pkgs)"
complete -c pkg -f -n "__fish_seen_subcommand_from install" -a "(__fish_pkg_complete_find_aur_pkgs)"
