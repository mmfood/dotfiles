function _fzf_man_pages

    set -f man_page (
        man -k . |
        _fzf_wrapper \
            --prompt="Man pages> " \
            --preview='man {1}{2} 2>/dev/null' \
            --query=(commandline) |
        string replace -r '(\w+)\s+\((\w+)\).*' 'man $2 $1'
    )

    if test $status -eq 0
        commandline --replace $man_page
    end

    commandline --function repaint
end
