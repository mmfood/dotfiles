set fish_greeting
fish_vi_key_bindings

function fish_user_key_bindings -d "My keybindings"
    bind -M insert \cy accept-autosuggestion
    bind -M insert \ct suppress-autosuggestion
end

# Tests
#
# test if <cmd> is something executable (function, builtin or external program)
# type -q <cmd>
#
# test if <cmd> is external program
# command -q <cmd>
#
# test if <cmd> is fish builtin
# builtin -q <cmd>

# test if <cmd> is fish function
# functions -q <cmd>

# == Variables ==#
#
# use bash for bspwm since fish is a bit wonky
set -U SXHKD_SHELL /usr/bin/bash

# == Useful functions ==#

# Edit config files
function cfg
    if set -l file (find -L ~/.config -maxdepth 2 -type f | fzf)
        $EDITOR $file
    end
end

# Edit dotfiles
function dots
    if set -l file (find -L ~/dotfiles -type f | fzf)
        $EDITOR $file
    end
end

# Look for file recursively in parents
function ancestor -a file
    set -l dir $PWD
    while [ $dir != "/" ]
        if [ -f $dir/$file ]
            printf "$dir/$file\n"
            return 0
        end
        set dir (dirname $dir)
        cd $dir
    end

    printf "%s not found in ancestors\n" $file
    return 1
end

# Search packages in Archlinux AUR
function aur
    echo "Searching for AUR packages: " $argv
    echo
    for pkg in $argv
        set title "Results for $pkg:"
        echo $title
        echo (string repeat -n (string length -V $title) '-')
        curl "https://aur.archlinux.org/packages?O=0&K=$pkg" 2> /dev/null \
            | grep "href=\"/packages/" | tr -d '<>"' | cut -d/ -f3
        echo (string repeat -n (string length -V $title) '-')
    end
end

# Move a dir from ~/.config to dotfiles/config and symlink it
function movecfg
    set -l cfg $argv[1]
    set -l src $XDG_CONFIG_HOME/$cfg
    set -l target ~/dotfiles/config/$cfg

    if [ -e $target ]
        printf "$target: already exists\n"
        return
    end

    if ! [ -e $src ]
        printf "$src: does not exist\n"
        return
    end

    mv $src $target
    ln -s $target $src
end

# Render a pdf from a r-markdown file
if command -q R
    function rmdtopdf
        echo 'require(rmarkdown); render("'(readlink -e $argv)'")' | R --vanilla
    end
end

# == abbreviations and aliases == #

abbr -a cc "clear && cd"
abbr -a cl "clear"

if command -q pacman
    # Check installed and running kernel version
    abbr -a kver "uname -r | sed -e 's/-lts//' && pacman -Q linux-lts | sed -e 's/linux-lts //'"
end

abbr -a o "xdg-open"

if command -q exa
    abbr -a l "exa"
    abbr -a la "exa -a"
    abbr -a ll "exa -l"
    abbr -a lla "exa -la"
    abbr -a lc "exa --oneline"
else
    abbr -a l "ls"
    abbr -a la "ls -a"
    abbr -a ll "ls -lh"
    abbr -a lla "ls -lha"
end

if command -q nvim
    abbr -a vim "nvim"
    abbr -a e "nvim"
    abbr -a vi "nvim"
end

if command -q bluetoothctl
    abbr -a bt "bluetoothctl"
end

# Docker
if command -q docker
    abbr -a dk "sudo docker"
end

# Git
if command -q git
    alias g "git"
    alias gs "git status"
    alias gc 'git commit -m'
    alias ga 'git add'
    alias gco 'git checkout'
    alias gp 'git push'
    alias gpl 'git pull'
end

# Cargo
if command -q cargo
    abbr -a ct "cargo test"
end

# CMake
if command -q cmake
    alias cmake-debug="cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=yes " # <script file>
end

# Irssi
# Currently using script to list usernames in separate tmux buffer
# so makes sence to alias irssi command to execute in a tmux buffer
# if command -qs irssi
#     abbr -a irssi "tmux new -s IRC -n irssi irssi"
# end

# Pipe stuff to ix.io paste bin e.g. cat <file> | pbin
alias pb="curl -s -F 'f:1=<-' ix.io"
# Store paste bin link in clipboard
if [ -n "$WAYLAND_DISPLAY" ]
    alias pbin="curl -s -F 'f:1=<-' ix.io | wl-copy"
else
    alias pbin "curl -s -F 'f:1=<-' ix.io | xsel -b -i"
end

alias suspend="systemctl suspend"
alias vpnstart="sudo systemctl restart openvpn-client@sthlm"
alias vpnstop="sudo systemctl stop openvpn-client@sthlm"
alias wgall="wget -r -np -nH --cut-dirs=3 -R" # Get all files from web dir
alias xcl="xclip -i -selection clipboard"

# == Tmux convenience functions == #

# List tmux session
function tls
    set -l sessions (tmux list-sessions 2>/dev/null | cut -d: -f1)
    if ! count $sessions &> /dev/null
        echo "No sessions"
    else
        printf "Active tmux sessions: \n(attach with tat <#> or tat <name>)\n\n"
        printf "#: name\n"
        printf "=======\n"
        for i in (seq (count $sessions))
            printf "%s: %s\n" $i $sessions[$i]
        end
    end
end

# Attach to tmux session
function tat -a id
    set -l sessions (tmux list-sessions 2>/dev/null | cut -d: -f1)
    if ! count $sessions &> /dev/null
        printf "No sessions\n"
    else
        if string match -q --regex '\d' $id
            printf "Attaching to %s\n" $sessions[$id]
            tmux attach -t $sessions[$id]
        else
            printf "Attaching to %s\n" $id
            tmux attach -t $id
        end
    end
end

# Create new tmux session
function tnew -a name
   tmux new -s $name
end

# == Fish git prompt == #

set __fish_git_prompt_showuntrackedfiles 'yes'
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate ''
set __fish_git_prompt_showupstream 'none'
set __fish_git_prompt_use_informative_chars 'yes'
set -g fish_prompt_pwd_dir_length 3

# Prompt
function fish_prompt
    set -l s $status
    # Time
    # set_color brwhite
    # printf "[%s]" (date +%H:%M)

    # Hostname
    # set_color blue
    # printf " %s" (hostname)

    # Directory
    if [ $PWD != $HOME ]
        set_color black
        printf ":"
        set_color yellow
        printf "%s" (basename $PWD)
    end
    set_color green
    printf "%s" (__fish_git_prompt)

    set_color red

    # Show status of last command
    if [ $s != 0 ]
        printf  "(%d) > " $s
    else
        printf  " > "
    end
    set_color normal
end

# function fish_mode_prompt
# end

function fish_mode_prompt
# function fish_right_prompt --description 'Displays the current mode to the right'
    if test "$fish_key_bindings" = "fish_vi_key_bindings"
        switch $fish_bind_mode
            case default
                set_color --bold red
                printf "[N]"
            case insert
                set_color --bold green
                printf "[I]"
            case replace-one
                set_color --bold green
                printf "[R1]"
            case replace
                set_color --bold green
                printf "[R]"
            case paste
                set_color --bold green
                printf "[P]"
            case visual
                set_color --bold brcyan
                printf "[V]"
            case "*"
                set_color --bold red
                printf "[?]"
        end
        set_color normal
        printf " "
    end
end

# colored man output
# # from http://linuxtidbits.wordpress.com/2009/03/23/less-colors-for-man-pages/
setenv LESS_TERMCAP_mb \e'[01;31m'       # begin blinking
setenv LESS_TERMCAP_md \e'[01;38;5;74m'  # begin bold
setenv LESS_TERMCAP_me \e'[0m'           # end mode
setenv LESS_TERMCAP_se \e'[0m'           # end standout-mode
setenv LESS_TERMCAP_so \e'[38;5;246m'    # begin standout-mode - info box
setenv LESS_TERMCAP_ue \e'[0m'           # end underline
setenv LESS_TERMCAP_us \e'[04;38;5;146m' # begin underline

setenv FZF_DEFAULT_COMMAND 'fd --type file --follow'
setenv FZF_CTRL_T_COMMAND 'fd --type file --follow'

if status --is-interactive
    if test (tty) = /dev/tty1
        Hyprland
    end
end
