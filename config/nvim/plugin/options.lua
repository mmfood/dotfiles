local set = vim.opt -- extended vim.o

-- History files
set.hidden = true
set.undofile = true
set.backup = false
set.writebackup = false

-- Spelling
set.spellcapcheck = ""

set.updatetime = 100

-- Appearence
set.cursorline = true
set.signcolumn = "yes"
set.colorcolumn = { 0 }
set.textwidth = 80
set.number = true
set.relativenumber = true
set.showcmd = true
set.showmode = false

if vim.fn.has("nvim-0.8") then
    set.laststatus = 3
else
    set.laststatus = 2
end

set.completeopt = "menuone,noselect,noinsert"

-- Window / tabs
set.splitright = true
set.splitbelow = true

-- Search
set.path = set.path + "**"
set.ignorecase = true --  Case insensitive searches (also applies to tags file
set.smartcase = true --  Case sensitive searches when using Uppercase words
set.scrolloff = 3 --  Search hits appear a few lines below the top
set.showmatch = true --  Jump to pair match and back at insert
set.incsearch = true --  Show incremental search match while typing

--[[ Special characters
   Use '»   ' for a tab of 4 columns (» = U00BB)
   Use · (U00B7) to show trailing spaces
   Use > when a line goes past the end of the window
   Use < when a line goeas past the beginning of the line
   Use ⎵ (U23B5) for non breaking space characters
--]]
set.list = true -- Always show list characters (above)
set.listchars = "tab:» >,trail:·,extends:>,precedes:<,nbsp:⎵"
set.conceallevel = 1

set.inccommand = "split"

-- Some default indentation settings
set.expandtab = true -- <Tab> key inserts spaces instead of tabs
set.softtabstop = 4 -- Number of columns to use for tab key
set.shiftwidth = 4 -- Number of columns to use for reindents
set.tabstop = 4 -- Number of columns used for an actual tab
set.shiftround = true -- Use even muliple of shiftwidth

set.clipboard = "unnamedplus"

vim.cmd("set shortmess+=I") -- No splash screen
vim.cmd("set shortmess-=W") -- Dont't show 'written' message afte :write
vim.cmd("set shortmess+=F") -- Dont't show filename in cmd window
vim.cmd("set shortmess-=c") -- No matching msgs from autocomplete menu

set.linebreak = true -- Break after !@*-+;:,./? instead of in words
set.showbreak = "⤷" -- Show symbol for wrapped lines

set.formatoptions = set.formatoptions
    - "a" -- no auto formatting
    - "t" -- Don't auto format my code. I got linters for that.
    + "c" -- In general, I like it when comments respect textwidth
    + "q" -- Allow formatting comments w/ gq
    - "o" -- O and o, don't continue comments
    + "r" -- But do continue when pressing enter.
    + "n" -- Indent past the formatlistpat, not underneath it.
    + "j" -- Auto-remove comments if possible.

set.joinspaces = false -- Do not add spaces when joining lines with J
set.comments = "sr:/*,mb:*,ex:*/" -- Comments are continued with newline
