local leader = " "
vim.g.mapleader = leader
vim.b.mapleader = leader

-- Turn of some stuff we're not using
vim.g.loaded_ruby_provider = 0
vim.g.loaded_node_provider = 0
vim.g.loaded_perl_provider = 0

vim.opt.termguicolors = true

require("pabo.globals")

-- Setup plugin manageer (Lazy.nvim)
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

vim.keymap.set("n", "<leader><leader>", "<cmd>Lazy<cr>", {
    desc = "Open Plugin Manager (Lazy)",
})

require("lazy").setup("pabo.plugins", {
    change_detection = {
        -- automatically check for config file changes and reload the ui
        enabled = true,
        notify = false, -- get a notification when changes are found
    },
    ui = {
        border = "rounded",
        title = "~ Plugins ~",
    },
})
