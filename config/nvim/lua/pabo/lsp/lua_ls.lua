return {
    on_init = function(client)
        local path = client.workspace_folders[1].name
        if vim.loop.fs_stat(path .. "/.luarc.json") and not vim.loop.fs_stat(path .. "/.luarc.jsonc") then
            return
        end
        client.config.settings.Lua = vim.tbl_deep_extend("force", client.config.settings, {
            runtime = {
                -- Tell the language server which version of Lua you're using
                -- (most likely LuaJIT in the case of Neovim)
                version = "LuaJIT",
            },
            -- Make the server aware of Neovim runtime files
            workspace = {
                checkThirdParty = false,
                library = {
                    vim.fn.expand("$VIMRUNTIME/lua"),
                    vim.fn.expand("$VIMRUNTIME/lua/vim/lsp"),
                },
                -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
                -- library = vim.api.nvim_get_runtime_file("", true)
            },
            diagnostics = {
                globals = { "vim", "prequire", "D" }, -- can be used to tell lua_ls about vim globals
            },
        })
        client.notify("workspace/didChangeConfiguration", { settings = client.config.settings })
    end,
    settings = {
        Lua = {
            -- inlay hints
            hint = {
                enable = true,
            },
        },
    },
}
