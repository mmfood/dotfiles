local lspconfig = prequire("lspconfig")
if not lspconfig then
    return {}
end

local root_pattern = lspconfig.util.root_pattern
local dirname = lspconfig.util.dirname
local clangd_extensions = prequire("clangd_extensions")
if clangd_extensions then
    clangd_extensions.setup({
        -- Note: this will only be anabled if nvim verson < 0.10
        -- otherwise neovim builtin inlay hint functions will be used
        -- by default
        inlay_hints = {
            only_current_line = true,
            max_len_align = true,
            parameter_hints_prefix = "𝑓 ",
            other_hints_prefix = " -> ",
            -- highlight = 'NonText',
            right_align = false,
        },
    })
    vim.api.nvim_create_autocmd("LspAttach", {
        group = vim.api.nvim_create_augroup(vim.env.USER .. "ClangdLspConfig", {}),
        callback = function()
            require("clangd_extensions.inlay_hints").setup_autocmd()
            require("clangd_extensions.inlay_hints").set_inlay_hints()
        end,
    })
end

return {
    cmd = {
        "clangd",
        "--compile-commands-dir=build",
        "--background-index",
        "--clang-tidy",
        "--header-insertion=iwyu",
        "--header-insertion-decorators",
        "-j=2",
        "--log=error", -- error, info or verbose
    },
    filetypes = { "c", "cpp", "objc", "objcpp" },
    root_dir = root_pattern("compile_commands.json", "projections.json", "compile_flags.txt", ".git") or dirname,
    on_attach = function(_, bufnr)
        if clangd_extensions then
            vim.keymap.set("n", "<leader>sh", "<cmd>ClangdSwitchSourceHeader<CR>", {
                buffer = bufnr,
                desc = "Switch between source file and header file",
            })
        else
            vim.notify("Could not load clangd extensions", vim.log.levels.WARN)
        end
    end,
}
