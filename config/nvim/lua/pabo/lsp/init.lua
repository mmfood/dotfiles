local M = {}

-- Logging
-- 0 - trace
-- 1 - debug
-- 2 - info
-- 3 - warn
-- 4 - error
vim.lsp.set_log_level(vim.log.levels.ERROR)

local border_style = "rounded"

M.servers = {
    lua_ls = {}, --require("pabo.lsp.lua_ls"),
    clangd = require("pabo.lsp.clangd"),
    rust_analyzer = {
        on_attach = function(_, bufnr)
            -- Note that the hints are only visible after rust-analyzer
            -- has finished loading and you have to edit the file to
            -- trigger a re-render.
            vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
        end,
    },
}

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
    border = border_style,
})

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
    border = border_style,
})

vim.diagnostic.config({
    float = { -- :h vim.lsp.util.open_floating_preview
        border = border_style,
        max_height = 50,
        max_width = 120,
    },
    virtual_text = {
        severity = vim.diagnostic.severity.ERROR,
    },
})

-- User commands
vim.api.nvim_create_user_command("LspClearLog", function()
    -- os.remove(vim.lsp.get_log_path())
    os.execute('printf "" > ' .. vim.lsp.get_log_path())
    print("Cleared LSP log file")
end, {
    desc = "Clear the LSP log",
    force = true,
})

-- Inlay hints is builtin to nvim 0.10
if vim.fn.has("nvim-0.10") then
    vim.keymap.set("n", "<leader>ti", function()
        vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled({ bufnr = 0 }))
    end, { desc = "Toggle inlay_hints" })
end

vim.api.nvim_create_autocmd("LspAttach", {
    group = vim.api.nvim_create_augroup(vim.env.USER .. "UserLspConfig", {}),
    callback = function(ev)
        -- Enable completion triggered by <c-x><c-o>
        vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

        local map = function(mode, lhs, rhs, desc)
            local opts = { buffer = ev.buf, desc = desc or "" }
            vim.keymap.set(mode, lhs, rhs, opts)
        end

        local lhs = {
            goto_def = "<CR>",
            goto_type = "<leader>lt",
            list_refs = "gr",
            ws_symbols = "<leader>lws",
            doc_symbols = "<leader>lds",
            incoming = "<leader>lI",
            outgoing = "<leader>lo",
            impl = "<leader>li",
        }

        local ok, telescope = pcall(require, "telescope.builtin")
        if ok then
            map("n", lhs.goto_def, telescope.lsp_definitions, "Goto definition or list in telescope")
            map("n", "gd", telescope.lsp_definitions, "Goto definition or list in telescope")
            map("n", lhs.goto_type, telescope.lsp_type_definitions, "Goto type definition or list in telescope")
            map("n", lhs.list_refs, telescope.lsp_references, "LSP List All References of Current Symbol")
            map("n", lhs.impl, telescope.lsp_implementations, "Lsp goto implementation or list in Telescope")

            map("n", lhs.incoming, telescope.lsp_incoming_calls, "LSP List Callers of Symbol")
            map("n", lhs.outgoing, telescope.lsp_outgoing_calls, "LSP List calls from Symbol")

            map("n", lhs.doc_symbols, telescope.lsp_document_symbols, "LSP List Document Symbols")
            map("n", lhs.ws_symbols, telescope.lsp_workspace_symbols, "LSP List All Symbols In Current Workspace")
        else
            map("n", lhs.goto_def, vim.lsp.buf.definition, "LSP Jump To Definition")
            map("n", lhs.goto_type, vim.lsp.buf.type_definition, "LSP Jumpt to Type Definition of Symbol")
            map("n", lhs.list_refs, vim.lsp.buf.references, "LSP List All References of Current Symbol")
            map("n", lhs.impl, vim.lsp.buf.implementation, "Lsp goto implementation or list in Telescope")

            map("n", lhs.incoming, vim.lsp.buf.incoming_calls, "LSP List Callers of Symbol")
            map("n", lhs.outgoing, vim.lsp.buf.outgoing_calls, "LSP List Callers from Symbol")

            map("n", lhs.doc_symbols, vim.lsp.buf.document_symbol, "LSP List Document Symbols")
            map("n", lhs.ws_symbols, vim.lsp.buf.workspace_symbol, "LSP List All Symbols In Current Workspace")
        end

        map("n", "gD", vim.lsp.buf.declaration, "LSP Jump To Declaration")
        map("n", "LI", vim.lsp.buf.implementation, "LSP List All Implementations of Symbol")

        map("n", "<leader>ca", vim.lsp.buf.code_action, "LSP Code Actions")
        map("n", "K", vim.lsp.buf.hover, "LSP Hover")
        map("n", "gn", vim.lsp.buf.rename, "LSP Rename")

        -- signature help displays the signature of something callable.
        -- It is basically a simpler hover
        map("n", "<M-k>", vim.lsp.buf.signature_help, "LSP Signature Help")

        vim.api.nvim_create_user_command("LspFormat", function()
            vim.lsp.buf.format({ async = true })
        end, {})
    end,
})

M.setup = function()
    local lspconfig = prequire("lspconfig")
    if not lspconfig then
        vim.notify("Could not find lspconfig", vim.log.levels.ERROR)
        return
    end

    local capabilities = {}
    local cmp_nvim_lsp = prequire("cmp_nvim_lsp")
    if not cmp_nvim_lsp then
        print("cmp_nvim_lsp wasn't loaded")
        return
    else
        capabilities = cmp_nvim_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities())
    end

    for name, config in pairs(M.servers) do
        config.capabilities = capabilities
        lspconfig[name].setup(config)
    end
end

return M
