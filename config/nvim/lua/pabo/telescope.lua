local Path = require("plenary.path")
local find = require("telescope.builtin").find_files
local ts = require("telescope.builtin")
local actions = require("telescope.actions")

local find_cwd = function()
    local opts = {
        find_command = {
            "fd",
            "--type",
            "file",
            "-E",
            "build/",
            "-E",
            "target/",
        },
    }
    find(opts)
end

local find_dotfiles = function()
    local opts = {
        cwd = Path:new(vim.env.HOME, "dotfiles"):absolute(),
        prompt_title = "~ Dotfiles ~",
        follow = true,
    }
    find(opts)
end

-- Find files in neovim config directories
local find_nvim = function()
    local opts = {
        cwd = Path:new(vim.fn.stdpath("config")):absolute(),
        prompt_title = "~ Nvim config ~",
        follow = true,
    }
    find(opts)
end

-- Find files in neovim plugin directories
local find_plugin = function()
    local opts = {
        cwd = Path:new(vim.fn.stdpath("data"), "lazy"):absolute(),
        prompt_title = "~ Plugins~",
        follow = true,
    }
    find(opts)
end

local grep_custom_dir = function()
    local cwd = vim.fn.input({
        prompt = "Grep in path: ",
        default = vim.fn.getcwd(),
        completion = "dir",
        cancelreturn = "",
    })
    if cwd == "" then
        return
    end
    ts.live_grep({ cwd = cwd, prompt_title = "~ Grep (" .. cwd .. ") ~" })
end

local grep_source_files = function()
    ts.live_grep({
        prompt_title = "~ Grep (Source Files) ~",
        glob_pattern = "!test/",
    })
end

local set = vim.keymap.set

set("n", "<C-p>", find_cwd, { desc = "Find files recursively in current directory" })
set("n", "<leader>fp", find_plugin, { desc = "Find files in neovim plugin directories" })
set("n", "<leader>fn", find_nvim, { desc = "Find files in neovim config directories" })
set("n", "<leader>fd", find_dotfiles, { desc = "Find files in $HOME/dotfiles" })

set("n", "<leader>fb", ts.buffers, { desc = "(Rip)Grep in current directory" })
set("n", "<leader>fg", ts.git_files, { desc = "List files tracked by Git" })
set("n", "<leader>lgh", function()
    ts.live_grep({ prompt_title = vim.fn.getcwd() })
end, { desc = "Live grep in CWD" })
set("n", "<leader>lgc", grep_custom_dir, { desc = "Live grep in custom dir" })
set("n", "<leader>lgs", grep_source_files, { desc = "Live grep (source) in CWD" })
set("n", "<leader>fs", ts.grep_string, { desc = "Search for string under cursor in current directory" })

-- Git
set("n", "<leader>gs", ts.git_status, { desc = "Git status" })
set("n", "<leader>gst", ts.git_stash, { desc = "Git stash" })
set("n", "<leader>gc", ts.git_commits, { desc = "Git commits" })
set("n", "<leader>gbc", ts.git_bcommits, { desc = "Git branch commits" })
set("n", "<leader>gb", ts.git_branches, { desc = "Git branches" })

-- Vim
set("n", "<leader>vo", ts.vim_options, { desc = "List Neovim Options" })
set("n", "<leader>vc", ts.commands, { desc = "List Neovim Commands" })
set("n", "<leader>vh", ts.highlights, { desc = "List Neovim highlights" })
set("n", "<leader>vj", ts.jumplist, { desc = "Show jumplist" })
set("n", "<leader>vk", ts.keymaps, { desc = "List keymaps" })
set("n", "<leader>vr", ts.registers, { desc = "List Registers" })
set("n", "<leader>tr", ts.reloader, { desc = "List / Reload lua modules" })
set("n", "<leader>fh", ts.help_tags, { desc = "List help tags" })

local action_layout = require("telescope.actions.layout")
local telescope_config = {
    defaults = {
        -- path_display = function(_, path)
        --     return path
        -- end,
        mappings = {
            i = {
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<C-p>"] = action_layout.toggle_preview,
            },
            n = {
                ["<C-p>"] = action_layout.toggle_preview,
            },
        },
    },
    extensions = {
        ["ui-select"] = {
            require("telescope.themes").get_dropdown({
                -- even more opts
            }),
        },
        fzf = {
            -- fzf stuff
        },
    },
}

local telescope = require("telescope")
telescope.setup(telescope_config)

telescope.load_extension("ui-select")
telescope.load_extension("fzf")
