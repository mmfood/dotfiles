return {
    {
        "catppuccin/nvim",
        name = "catppuccin",
        lazy = true, -- main colorscheme
        priority = 1000,
        opts = {
            no_italic = true, -- Force no italic
            no_bold = true, -- Force no bold
            no_underline = false, -- Force no underline
            transparent_background = false,
            integrations = {
                cmp = true,
            },
        },
        config = function(_, opts)
            require("catppuccin").setup(opts)
            vim.cmd.colorscheme("catppuccin")
        end,
    },
    "cranberry-clockworks/coal.nvim",
    {
        "tjdevries/colorbuddy.nvim",
        config = function()
            vim.cmd.colorscheme("gruvbuddy")
        end,
    },
    "folke/tokyonight.nvim",
    "RRethy/base16-nvim",
    {
        "stevearc/conform.nvim",
        opts = {
            formatters_by_ft = {
                cpp = { "clang_format" },
                lua = { "stylua" },
            },
            format_on_save = {
                timeout_ms = 500,
                lsp_fallback = true,
            },
        },
    },
}
