return {
    {
        "nvim-telescope/telescope.nvim",
        dependencies = { "nvim-lua/plenary.nvim" },
        config = function()
            require("pabo.telescope")
        end,
    },
    "nvim-telescope/telescope-ui-select.nvim", -- use telescope to select items
    {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
    },
}
