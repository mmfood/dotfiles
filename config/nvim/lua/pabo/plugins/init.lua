return {
    "tpope/vim-abolish", -- word substitutions etc e.g. crc to change to CamelCase or crs to change to snake_case
    {
        "tpope/vim-commentary", -- Easy commenting e.g. gcc to comment line, or gcaf to comment entire (around) function
        cond = not vim.fn.has("nvim-0.10"), -- Builtin support in nvim 0.10
    },
    "tpope/vim-dispatch", -- Make integration
    "tpope/vim-projectionist", -- Project specific configs
    "tpope/vim-repeat", -- use . for repeating tpope commands
    "tpope/vim-surround", -- Easy surround with symbols e.g. gsw" to surround word with "" or cs)" to change surrounding () with ""
    "tpope/vim-unimpaired", -- Useful mappings with [ and ]
    -- "christoomey/vim-tmux-navigator", -- Easy navigation vim <-> tmux
    "christoomey/vim-sort-motion", -- Easy sort paragraphs
    {
        "kana/vim-textobj-entire", -- eg die to delete entire buffer
        dependencies = { "kana/vim-textobj-user" }, -- Create custom text objects
    },
    "windwp/nvim-autopairs",
    {
        "lukas-reineke/indent-blankline.nvim", -- Show indent level
        main = "ibl",
        opts = { scope = { enabled = false } },
    },
    "milisims/nvim-luaref",
    {
        "folke/todo-comments.nvim",
        dependencies = { "nvim-lua/plenary.nvim" },
        config = true,
    },
    -- {
    --     "ahmedkhalf/project.nvim",
    --     dependencies = { "nvim-telescope/telescope.nvim" },
    --     opts = {
    --         patterns = { ".git", ">~/dotfiles/config", "gradlew", "mvnw", "Makefile", "package.json", "go.mod" },
    --     },
    --     config = function(_, opts)
    --         require("project_nvim").setup(opts)
    --         require("telescope").load_extension("projects")
    --     end,
    -- },
}
