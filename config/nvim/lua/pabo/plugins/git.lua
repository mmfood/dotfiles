return {
    {
        "tpope/vim-fugitive", -- Git integration
        config = function()
            vim.keymap.set("n", "<leader>gd", "<cmd>Gvdiffsplit!<cr>", { desc = "Threeway diffsplit" })
            vim.keymap.set("n", "<leader>gs", "<cmd>Git<cr>", { desc = "Threeway diffsplit" })
        end,
    },
    {
        -- TODO move this to separate git.lua file
        "lewis6991/gitsigns.nvim", -- Git gutters and hunk textobjects etc
        config = function()
            vim.api.nvim_set_hl(0, "GitSignsChange", { fg = "#2188ff" })
            local gs = require("gitsigns")
            gs.setup()
            vim.keymap.set("n", "]h", gs.next_hunk, { desc = "Jump to Next Git Hunk" })
            vim.keymap.set("n", "[h", gs.prev_hunk, { desc = "Jump to Previous Git Hunk" })
            vim.keymap.set("n", "gh", gs.preview_hunk, { desc = "Show Git Hunk Under Cursor" })
        end,
    },
}
