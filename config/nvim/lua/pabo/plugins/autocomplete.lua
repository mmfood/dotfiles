---@diagnostic disable: missing-fields
local get_comparators = function()
    local cmp = require("cmp")
    local clangd_extensions = prequire("clangd_extensions.cmp_scores")
    if clangd_extensions then
        return {
            cmp.config.compare.offset,
            cmp.config.compare.exact,
            cmp.config.compare.recently_used,
            clangd_extensions,
            cmp.config.compare.kind,
            cmp.config.compare.sort_text,
            cmp.config.compare.length,
            cmp.config.compare.order,
        }
    else
        return {
            cmp.config.compare.offset,
            cmp.config.compare.exact,
            cmp.config.compare.recently_used,
            cmp.config.compare.kind,
            cmp.config.compare.sort_text,
            cmp.config.compare.length,
            cmp.config.compare.order,
        }
    end
end

return {
    {
        "hrsh7th/nvim-cmp",
        dependencies = { "onsails/lspkind.nvim" },
        config = function()
            local cmp = require("cmp")
            local lspkind = require("lspkind")

            cmp.setup({
                snippet = {
                    expand = function(args)
                        local luasnip = prequire("luasnip")
                        if not luasnip then
                            print("Luasnip could not be loaded")
                            return
                        end
                        luasnip.lsp_expand(args.body)
                    end,
                },
                window = { -- :h cmp-config.views and :h nvim_open_win
                    completion = {
                        border = "rounded",
                        max_height = 50,
                    },
                    documentation = {
                        border = "rounded",
                        max_height = 50,
                    },
                },
                mapping = {
                    ["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
                    ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),
                    ["<C-n>"] = cmp.mapping.select_next_item(),
                    ["<C-p>"] = cmp.mapping.select_prev_item(),
                    ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
                    ["<C-y>"] = cmp.mapping.confirm({
                        behavior = cmp.ConfirmBehavior.Insert,
                        select = true, -- No selected item? true complete first item, false: don't complete
                    }),
                    ["<C-e>"] = cmp.mapping({
                        i = cmp.mapping.abort(),
                        c = cmp.mapping.close(),
                    }),
                },
                -- each argument to cmp.config.sources is a separate group
                -- The first group has priority over the second and so on.
                -- If a match is found in the first grup no more groups will be shown
                sources = cmp.config.sources({
                    { name = "luasnip" },
                    { name = "nvim_lsp" }, -- complete with LSP
                    { name = "nvim_lua" }, -- will only be active in lua files
                    { name = "nvim_lsp_signature_help" }, -- add signature to completion
                }, { -- this group will not be shown if the above has any completions
                    { name = "buffer", keyword_length = 5 }, -- complete with words in buffer
                    { name = "path" }, -- complete paths
                }),
                formatting = {
                    format = lspkind.cmp_format({
                        node = "symbol_test",
                        menu = {
                            nvim_lsp = "[LSP]",
                            nvim_lua = "[lua]",
                            luasnip = "[snip]",
                            nvim_signature_help = "[help]",
                            buffer = "[buf]",
                            path = "[path]",
                        },
                    }),
                },
                sorting = {
                    comparators = get_comparators(),
                },
                experimental = {
                    ghost_text = true, -- show selected completion on the line as virtual text
                },
            })
        end,
    },
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-cmdline",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-nvim-lua",
    "saadparwaiz1/cmp_luasnip",
    "hrsh7th/cmp-nvim-lsp-signature-help",
}
