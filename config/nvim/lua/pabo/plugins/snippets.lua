return {
    "L3MON4D3/LuaSnip",
    -- follow latest release.
    version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
    -- install jsregexp (optional!).
    build = "make install_jsregexp",
    config = function()
        local ls = prequire('luasnip')
        if not ls then
            return
        end

        ls.config.set_config {
            history = true, -- Makes it possible to jump back into snuppets afer moving outside selection
            updateevents = 'TextChanged,TextChangedI',
            store_selection_keys = '<Tab>',
            -- Autosnippets
            -- enable_autosnippets = true,
        }

        vim.keymap.set({'i', 's'}, '<c-k>', function()
            if ls.expand_or_jumpable() then
                ls.expand_or_jump()
            end
        end, {silent = true})

        vim.keymap.set({'i', 's'}, '<c-j>', function()
            if ls.jumpable(-1) then
                ls.jump(-1)
            end
        end, {silent = true})

        -- select within a list of options
        -- useful for choice nodes
        vim.keymap.set({'i', 's'}, '<c-l>', function()
            if ls.choice_active() then
                ls.change_choice(1)
            end
        end)

        vim.keymap.set('n', '<leader><leader>s', '<cmd>source ~/.config/nvim/after/plugin/luasnip.lua<CR>')
        vim.keymap.set('i', '<c-u>', require 'luasnip.extras.select_choice')
    end,
}
