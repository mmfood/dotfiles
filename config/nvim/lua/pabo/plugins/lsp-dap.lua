--[ LSP and DAP (Debugger Adapter Protocol) ]--

return {
    {
        "neovim/nvim-lspconfig", -- Starting configs for lsp
        dependencies = {
            {
                "folke/neodev.nvim",
                opts = {
                    override = function(root_dir, options)
                        -- D(root_dir)
                        -- D(options)
                    end,
                },
            },
            {
                "williamboman/mason-lspconfig.nvim",
                dependencies = {
                    {
                        "williamboman/mason.nvim",
                        cmd = "Mason",
                        opts = {
                            ensure_installed = {
                                "stylua", -- lint can be installed via cargo
                                "cpptools", -- dap
                            },
                        },
                    },
                },
                opts = {
                    ensure_installed = {
                        "jdtls",
                        "bashls",
                        "gopls",
                        "lua_ls",
                        "rust_analyzer",
                        "vimls",
                    },
                },
            },
        },
        config = function()
            require("pabo.lsp").setup() -- start lsp config after masin
        end,
    },
    { -- DAP protocol for nvim
        "mfussenegger/nvim-dap",
        config = function()
            require("pabo.dap")
        end,
    },
    "mfussenegger/nvim-jdtls", -- loaded by ftplugin
    "p00f/clangd_extensions.nvim",
    {
        "j-hui/fidget.nvim",
        opts = {},
    },
}
