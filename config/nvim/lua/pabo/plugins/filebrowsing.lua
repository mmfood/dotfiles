local HEIGHT_RATIO = 0.8 -- You can change this
local WIDTH_RATIO = 0.5 -- You can change this too

local window = function() end

return {
    -- {
    --     "nvim-tree/nvim-tree.lua",
    --     lazy = false,
    --     dependencies = {
    --         "nvim-tree/nvim-web-devicons",
    --     },
    --     config = function()
    --         vim.keymap.set("n", "<C-f>", ":NvimTreeToggle<CR>")
    --         require("nvim-tree").setup({
    --             view = {
    --                 float = {
    --                     enable = true,
    --                     open_win_config = function()
    --                         local screen_w = vim.opt.columns:get()
    --                         local screen_h = vim.opt.lines:get() - vim.opt.cmdheight:get()
    --                         local window_w = screen_w * WIDTH_RATIO
    --                         local window_h = screen_h * HEIGHT_RATIO
    --                         local window_w_int = math.floor(window_w)
    --                         local window_h_int = math.floor(window_h)
    --                         local center_x = (screen_w - window_w) / 2
    --                         local center_y = ((vim.opt.lines:get() - window_h) / 2) - vim.opt.cmdheight:get()
    --                         return {
    --                             border = "rounded",
    --                             relative = "editor",
    --                             row = center_y,
    --                             col = center_x,
    --                             width = window_w_int,
    --                             height = window_h_int,
    --                         }
    --                     end,
    --                 },
    --                 width = function()
    --                     return math.floor(vim.opt.columns:get() * WIDTH_RATIO)
    --                 end,
    --             },
    --             -- Begin Project.nvim compatible settings
    --             sync_root_with_cwd = true,
    --             respect_buf_cwd = true,
    --             update_focused_file = {
    --                 enable = true,
    --                 update_root = true,
    --             },
    --             -- End Project.nvim compatible settings
    --         })
    --         require("nvim-web-devicons").setup({})
    --     end,
    -- },
    {
        "stevearc/oil.nvim",
        dependencies = { "nvim-tree/nvim-web-devicons" },
        opts = {
            columns = {
                "icon",
            },
            keymaps = {
                ["<C-h>"] = false,
                ["<M-h>"] = "actions.select_split",
                ["<C-l>"] = false,
                ["<M-l>"] = "actions.refresh",
            },
            view_options = {
                show_hidden = true,
            },
        },
        config = function(_, opts)
            require("oil").setup(opts)

            vim.keymap.set("n", "+", "<CMD>Oil<CR>", { desc = "Open parent directory" })
            vim.keymap.set("n", "<leader>+", require("oil").toggle_float, { desc = "Open parent directory" })
        end,
    },
}
