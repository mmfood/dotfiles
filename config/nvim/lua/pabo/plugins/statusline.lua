local myprogress = function()
    local cur = vim.fn.line(".")
    local last = vim.fn.line("$")
    if cur == 1 then
        return "Top"
    end
    if cur == last then
        return "Bot"
    end
    return string.format("%2d%%%%/%d", math.floor(cur / last * 100), last)
end

local function current_directory()
    local cwd = vim.fn.getcwd()
    return vim.fn.fnamemodify(cwd, ":~")
end

local function non_unixff()
    local ff = vim.o.fileformat
    if ff ~= "unix" then
        return "[" .. ff .. "]"
    end
    return ""
end

return {
    "hoob3rt/lualine.nvim",
    dependencies = {
        "nvim-tree/nvim-web-devicons",
        "tjdevries/colorbuddy.nvim",
    },
    config = function()
        local theme = nil
        if not vim.g.colors_name then
            -- Make section c more readable for default colorscheme
            theme = require("lualine.themes.auto")
            for _, mode in pairs({ "normal", "visual", "command", "insert", "inactive", "replace", "terminal" }) do
                local section = theme[mode]
                if section then
                    theme[mode].c.fg = "#000000"
                else
                    vim.notify("Did not find " .. mode)
                end
            end
        end

        require("lualine").setup({
            options = {
                icons_enabled = true,
                -- theme = theme,
                disabled_filetypes = { "NvimTree" },
                globalstatus = true,
                section_separators = "",
                component_separators = "",
            },
            sections = {
                lualine_c = {
                    {
                        "filename",
                        path = 1,
                    },
                },
                lualine_x = { current_directory, non_unixff, "filetype" },
                lualine_y = { "location" },
                lualine_z = { myprogress },
            },
            winbar = {},
            inactive_winbar = {},
        })
    end,
}
