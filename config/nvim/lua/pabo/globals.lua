-- Dump lua stuff with :lua dump("whatever")

-- Dump
_G.D = function(v)
    -- local objects = vim.tbl_map(vim.inspect, {...})
    -- print(unpack(objects))
    print(vim.inspect(v))
end

-- Require wrapper
-- returns the required module or nil on failure
_G.prequire = function(name)
    local ok, module = pcall(require, name)
    if not ok then
        return nil
    end
    return module
end
