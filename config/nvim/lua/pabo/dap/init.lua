local dap = prequire("dap")
if not dap then
    return
end

local api = vim.api

local widgets = require("dap.ui.widgets")

vim.api.nvim_create_autocmd("FileType", {
    pattern = { "dap-float" },
    desc = "Quit dap floating windows with q",
    callback = function()
        vim.keymap.set("n", "q", function()
            vim.api.nvim_win_close(0, true)
        end, { silent = true })
    end,
})

vim.keymap.set("n", "<leader>db", function()
    dap.toggle_breakpoint()
end)
vim.keymap.set("n", "<leader>dB", function()
    dap.continue()
end)
vim.keymap.set("n", "<leader>do", function()
    dap.step_over()
end)
vim.keymap.set("n", "<leader>di", function()
    dap.step_into()
end)
vim.keymap.set("n", "<leader>dO", function()
    dap.step_out()
end)
vim.keymap.set("n", "<leader>dr", function()
    dap.repl_open()
end)
vim.keymap.set("n", "<leader>dl", function()
    dap.run_last()
end)
vim.keymap.set("n", "<leader>du", function()
    dap.up()
end)
vim.keymap.set("n", "<leader>dU", function()
    dap.down()
end)

-- maybe use {'n', 'v'}
vim.keymap.set("n", "<leader>dh", function()
    widgets.hover()
end)
vim.keymap.set("n", "<leader>dp", function()
    widgets.preview()
end)
vim.keymap.set("n", "<leader>df", function()
    widgets.centered_float(widgets.frames)
end)

-- Show stack frames (stack trace)
local scopes_w = nil
vim.keymap.set("n", "<leader>ds", function()
    if not scopes_w then
        scopes_w = widgets.sidebar(widgets.frames)
    end
    scopes_w.toggle()
end)

-- Show local variables
local scopes_w = nil
vim.keymap.set("n", "<leader>ds", function()
    if not scopes_w then
        scopes_w = widgets.sidebar(widgets.scopes, { width = 50 })
    end
    scopes_w.toggle()
end)

-- Evaluate expressions
-- Enter the window and enter an expression in insert mode
-- will be evaluated every stop
local expression_w = nil
vim.keymap.set("n", "<leader>de", function()
    -- FIX this only evaluates expression under cursor. Use dap-ui instead
    if not expression_w then
        expression_w = widgets.sidebar(widgets.expression)
    end
    expression_w.toggle()
end)

dap.adapters.cppdbg = {
    id = "cppdbg",
    type = "executable",
    command = vim.fn.stdpath("data") .. "/mason/bin/OpenDebugAD7",
}
dap.configurations.cpp = {
    {
        name = "Launch",
        type = "cppdbg", -- must match a dap.adapters.<type>
        request = "launch",
        program = function()
            return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
        end,
        cwd = "${workspaceFolder}",
        stopAtEntry = true,
        externalConsole = false,
        setupCommands = {
            {
                text = "-enable-pretty-printing",
                description = "enable pretty printing",
                ignoreFailures = false,
            },
        },
        MIMode = "gdb",
    },
}

-- Event handling
-- https://microsoft.github.io/debug-adapter-protocol/specification#Events_Initialized
--
dap.listeners.after["event_initialized"]["me"] = function()
    vim.notify("DAP: Started!", vim.log.levels.INFO)
end

dap.listeners.after["event_terminated"]["me"] = function(session, body)
    vim.notify("DAP: Terminated!", vim.log.levels.INFO)
    print("Session terminated", vim.inspect(session), vim.inspect(body))
end

local keymap_restore = {}
dap.listeners.after["event_initialized"]["me"] = function()
    for _, buf in pairs(api.nvim_list_bufs()) do
        local keymaps = api.nvim_buf_get_keymap(buf, "n")
        for _, keymap in pairs(keymaps) do
            if keymap.lhs == "K" then
                table.insert(keymap_restore, keymap)
                api.nvim_buf_del_keymap(buf, "n", "K")
            end
        end
    end
    api.nvim_buf_set_keymap(0, "n", "K", "", {
        callback = function()
            widgets.hover()
        end,
        silent = true,
    })
end

dap.listeners.after["event_terminated"]["me"] = function()
    for _, keymap in pairs(keymap_restore) do
        vim.notify("Restoring keymaps")
        api.nvim_buf_set_keymap(
            keymap.buffer,
            keymap.mode,
            keymap.lhs,
            type(keymap.rhs) == "string" and keymap.rhs or "",
            {
                silent = keymap.silent == 1,
                callback = keymap.callback,
            }
        )
    end
    keymap_restore = {}
end
