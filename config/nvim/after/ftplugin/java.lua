local find_launcher = function(launcher_path)
    local launchers = vim.fn.glob(launcher_path .. "plugins/org.eclipse.equinox.launcher_*jar", false, true)

    if #launchers > 1 then
        vim.notify("Found more than one launcher in " .. launcher_path, vim.log.levels.WARN)
        return nil
    elseif #launchers < 1 then
        vim.notify("Found no launcher in " .. launcher_path, vim.log.levels.WARN)
        return nil
    end
    local jar_path = launchers[1]
    vim.notify("Found launcher: " .. jar_path, vim.log.levels.DEBUG)
    return jar_path
end

local find_jdebug_jar = function(jdebug_path)
    local jars = vim.fn.glob(jdebug_path .. "/extension/server/com.microsoft.java.debug.plugin-*.jar", false, true)

    if #jars > 1 then
        vim.notify("Found more than one jdebugger in " .. jdebug_path, vim.log.levels.WARN)
        -- return first
    elseif #jars < 1 then
        vim.notify("Found no launcher in " .. jdebug_path, vim.log.levels.WARN)
        return nil
    end
    local jar = jars[1]
    return jar
end

local setup_keymaps = function(bufnr)
    local jdtls = require("jdtls")

    local map = function(mode, lhs, rhs, opts)
        opts = opts or {}
        opts.buffer = bufnr
        opts.silent = true
        vim.keymap.set(mode, lhs, rhs, opts)
    end

    local vextract = function(vtype)
        return function()
            local name = vim.fn.input("Enter new " .. vtype .. " name: ")
            if not type(name) == "string" or name == "" then
                print("Strange name: " .. name)
                return
            end

            local opts = { visual = true, name = name }
            if vtype == "variable" then
                jdtls.extract_variable(opts)
            elseif vtype == "constant" then
                jdtls.extract_constant(opts)
            elseif vtype == "method" then
                jdtls.extract_method(opts)
            else
                vim.notify("Unexpected extaction type" .. vtype)
            end
        end
    end

    -- JDTLS
    map("n", "<leader>jo", jdtls.organize_imports, { desc = "[JDTLS] Organize Imports" })
    map("n", "<leader>jv", jdtls.extract_variable, { desc = "[JDTLS] Extract variable" })
    map("n", "<leader>jc", jdtls.extract_constant, { desc = "[JDTLS] Extract constant" })
    map("n", "<leader>ju", jdtls.update_projects_config, { desc = "[JDTLS] Update Project Config" })

    map("v", "<leader>jv", vextract("variable"), { desc = "[JDTLS] Extract variable" })
    map("v", "<leader>jc", vextract("constant"), { desc = "[JDTLS] Extract constant" })
    map("v", "<leader>jm", vextract("method"), { desc = "[JDTLS] Extract method" })

    -- DAP
    map("n", "<leader>jt", jdtls.test_nearest_method, { desc = "[JDTLS] Test method" })
    map("n", "<leader>jT", jdtls.test_class, { desc = "[JDTLS] Test class" })
end

local setup_jdtls = function()
    local jdtls = prequire("jdtls")
    if not jdtls then
        vim.notify("Jdtls not loaded", vim.log.levels.WARN)
        return
    end

    local jdtls_setup = require("jdtls.setup")

    local mason_pkg_path = vim.fn.stdpath("data") .. "/mason/packages"
    local jdtls_path = mason_pkg_path .. "/jdtls/"
    local path_to_jdebug = mason_pkg_path .. "/java-debug-adapter"
    local path_to_jtest = mason_pkg_path .. "/java-test"

    local config_path = jdtls_path .. "/config_linux"
    local jdtls_jar_path = find_launcher(jdtls_path)
    if not jdtls_jar_path then
        vim.notify("Starting JDTLS failed", vim.log.levels.WARN)
        return
    end

    local jdebug_bundle = find_jdebug_jar(path_to_jdebug)
    if not jdebug_bundle then
        vim.notify("Starting JDTLS failed", vim.log.levels.WARN)
        return
    end
    local bundles = { jdebug_bundle }
    vim.list_extend(bundles, vim.fn.glob(path_to_jtest .. "/extension/server/*.jar", false, true))

    local extendedClientCapabilities = jdtls.extendedClientCapabilities
    extendedClientCapabilities.resolveAdditionalTextEditsSupport = true

    local config = {}
    config.init_options = {
        bundles = bundles,
        extendedClientCapabilities = extendedClientCapabilities,
    }

    local root_markers = { ".git", "mvnw", "gradlew", "pom.xml", "build.gradle" }
    local root_dir = jdtls_setup.find_root(root_markers)

    local project_name = vim.fn.fnamemodify(root_dir, ":p:h:t")
    local workspace_dir = vim.env.HOME .. "/.cache/jdtls/workspace/" .. project_name

    config.settings = {
        java = {
            signatureHelp = { enabled = true },
            configuration = {
                runtimes = {
                    {
                        name = "JavaSE-1.8",
                        path = "/usr/lib/jvm/java-8-openjdk",
                    },
                    {
                        name = "JavaSE-11",
                        path = "/usr/lib/jvm/java-11-openjdk",
                    },
                    {
                        name = "JavaSE-21",
                        path = "/usr/lib/jvm/java-21-openjdk",
                    },
                },
            },
        },
    }
    config.cmd = {

        -- 💀
        "java", -- or '/path/to/java17_or_newer/bin/java'
        -- depends on if `java` is in your $PATH env variable and if it points to the right version.

        "-Declipse.application=org.eclipse.jdt.ls.core.id1",
        "-Dosgi.bundles.defaultStartLevel=4",
        "-Declipse.product=org.eclipse.jdt.ls.core.product",
        "-Dlog.protocol=true",
        "-Dlog.level=ALL",
        "-Xmx1g",
        "--add-modules=ALL-SYSTEM",
        "--add-opens",
        "java.base/java.util=ALL-UNNAMED",
        "--add-opens",
        "java.base/java.lang=ALL-UNNAMED",

        -- 💀
        "-jar",
        jdtls_jar_path,
        -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                       ^^^^^^^^^^^^^^
        -- Must point to the                                                     Change this to
        -- eclipse.jdt.ls installation                                           the actual version

        -- 💀
        "-configuration",
        config_path,
        -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        ^^^^^^
        -- Must point to the                      Change to one of `linux`, `win` or `mac`
        -- eclipse.jdt.ls installation            Depending on your system.

        -- 💀
        -- See `data directory configuration` section in the README
        "-data",
        workspace_dir,
    } -- handled by Mason
    config.root_dir = root_dir

    config.on_attach = function(_, bufnr)
        setup_keymaps(bufnr)
        jdtls.setup_dap()
        jdtls.setup.add_commands() -- not needed?
    end

    jdtls.start_or_attach(config)
end

setup_jdtls()
