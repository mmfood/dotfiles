print("Sourcing lua ftplugin")
local group = vim.api.nvim_create_augroup("pabo_rust", { clear = true })
vim.api.nvim_create_autocmd("BufWritePre", {
    group = group,
    pattern = "<buffer>",
    desc = "Format rust files on save 2",
    callback = function()
        vim.lsp.buf.format({ async = true })
    end,
})
