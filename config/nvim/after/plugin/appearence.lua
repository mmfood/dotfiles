-- Flash highlight yanked text
vim.api.nvim_create_autocmd('TextYankPost', {
    pattern = "*",
    desc = "Highlight yanked text",
    callback = function(_)
        vim.highlight.on_yank({
            higroup = "IncSearch",
            timeout = 150,
        })
    end,
})
