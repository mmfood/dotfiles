--[ Git stuff ]--


-- General:
--
-- Use :Git to show status in a new window and g? to see mappings
--
-- Can be used to stage/unstage files, commit changes etc
--
-- Use :Git <git command> <args>... as in the terminal but with neovim integration
-- for things like showing the log, quickfix lists for diff hunks/files etc


-- Merge conflicts with Fugitive:
--
-- + Open the file with conflicts, perhaps from the status window (:Git)
--
-- + Open the 3-way diff with :Gdiffsplit! <--- note the ! otherwise it will be a
--   2 way split
--
-- + The left side is the HEAD/target (theirs) and the right side is the merge (ours)
--
-- + Naviagate the conflicts with [c and ]c
--
-- + To select from the left (theirs): d2o   or :diffget //2
-- + To select from the rigth (ours):  d3o   or :diffget //3
--
-- + To select entire file from left or right, move to the window and :Gwrite!
--
-- + To exit from the diff view close the left and right side: <C-w><C-o>
