" Help File Speedups
" thanks Chris Tooomey
"-------------------

au filetype help call HelpFileMode()

function! HelpFileMode()
    wincmd _ " Maximze the help on open

    " Tab jumps between keywords, Shift-Tab jumps in reverse
    nnoremap <buffer> <tab> :call search('\|.\{-}\|', 'w')<cr>:noh<cr>2l
    nnoremap <buffer> <S-tab> F\|:call search('\|.\{-}\|', 'wb')<cr>:noh<cr>2l

    " Enter follows link to reference
    nnoremap <buffer> <cr> <c-]>

    " Backspace jumps back to link parent location
    nnoremap <buffer> <bs> <c-T>

    " q quits
    nnoremap <buffer> q :q<CR>

    " Remove linenumber
    setlocal nonumber
endfunction
