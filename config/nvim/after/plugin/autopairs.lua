local autopairs = prequire("nvim-autopairs")
if not autopairs then
    return
end

autopairs.setup {
   fast_wrap = {
       pattern = [=[[%'%"%)%>%]%)%}%,;]]=],  -- end points ?
       chars = { '{', '[', '(', '"', "'", '<', },
   },
}

local cmp = prequire('cmp')
if not cmp then
    return
end

local cmp_autopairs = prequire('nvim-autopairs.completion.cmp')
if not cmp_autopairs then
    return
end

cmp.event:on(
  'confirm_done',
  cmp_autopairs.on_confirm_done()
)
