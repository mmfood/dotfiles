--[ General Key Maps ]--

local set = vim.keymap.set

-- Fix nordic keys
-- Using silent = false so they apply immediately
set("n", "-", "/", { noremap = false, silent = false })
set("n", "Ö", ":", { noremap = false, silent = false })
set("n", "ö", ";", { noremap = false, silent = false })
set("n", "ä", "'", { noremap = false, silent = false })
set("n", "Ä", '"', { noremap = false, silent = false })
set("n", "§", "`", { noremap = false, silent = false })
set("i", "§", "`", { noremap = false, silent = false })

if not vim.fn.has("nvim-0.10") then
    set("n", "<C-w><C-d>", vim.diagnostic.open_float, { desc = "Open Diagnostic Floating Window" })
    set("n", "<leader>lD", vim.diagnostic.show)

    set("n", "[d", vim.diagnostic.goto_prev)
    set("n", "]d", vim.diagnostic.goto_next)
end

-- Make vim-unimpaired mappings more useful on nordic keyboards
set("n", "å", "[", { remap = true })
set("n", "~", "]", { remap = true })

-- -- Avoid Ex mode
set("n", "Q", "q", { noremap = false, desc = "Avoid ex mode" })

-- -- Yank to the end of the line, similar to D and C (all modes)
set("", "Y", "y$", { desc = "Yank to the end of line" })

-- -- Use Ctrl-A and Ctrl-E for navigating the command line
set("c", "<c-a>", "<Home>", { noremap = false, silent = false, desc = "Go to beginning of command line" })
set("c", "<c-e>", "<End>", { noremap = false, silent = false, desc = "Go to end of command line" })

-- -- Open new splits  and tabs
set("n", "<Leader><S-t>", "<cmd>tabnew<cr>", { desc = "Open new tab" })
set("n", "<Leader><S-l>", "<cmd>wincmd v<CR>", { desc = "Split window vertically" })
set("n", "<Leader><S-k>", "<cmd>wincmd s<CR>", { desc = "Split window horizontally" })

-- Change window size
set("n", "<M-,>", "<cmd>wincmd 5 < <CR>", { desc = "Decrease win width" })
set("n", "<M-.>", "<cmd>wincmd 5 > <CR>", { desc = "Increase win width" })
set("n", "<M-t>", "<cmd>wincmd 5 + <CR>", { desc = "Decrease win height" })
set("n", "<M-s>", "<cmd>wincmd 5 - <CR>", { desc = "Increase win height" })

set("n", "<Escape>", function()
    local force = false
    local wins = vim.fn.getwininfo()
    for _, w in pairs(wins) do
        if w.quickfix == 1 or w.loclist == 1 then
            vim.api.nvim_win_close(w.winid, force)
        end
    end
end, { desc = "Close QuickFix/Location List, If Open" })
