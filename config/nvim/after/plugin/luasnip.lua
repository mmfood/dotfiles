local ls = prequire'luasnip'
if not ls then
    return
end

-- while debugging snippets...
ls.cleanup()

-- https://github.com/L3MON4D3/LuaSnip/blob/master/DOC.md
local s = ls.snippet
local sn = ls.snippet_node
--local t = ls.text_node
local i = ls.insert_node
--local isn = ls.indent_snippet_node
local f = ls.function_node
local fmt = require("luasnip.extras.fmt").fmta
local c = ls.choice_node
local d = ls.dynamic_node
--local r = ls.restore_node

local selected = function(indent)
    return function(_, parent)
        local env = parent.snippet.env
        local lines = {}
         for _, line in ipairs(env.LS_SELECT_DEDENT) do
             table.insert(lines, indent..line)
         end
         return lines
    end
end

local selected_or_body = function(_, parent)
    local env = parent.snippet.env
    -- LS_SELECT_DEDENT is a string when generatinc docstring
    -- and is an empty table if nothing was selected
    if type(env.LS_SELECT_DEDENT) == "string" or next(env.LS_SELECT_DEDENT) == nil then
        return sn(1, {i(1, "{body}")})
    end
    return sn(1, { f(selected("")), i(1) })
end

ls.add_snippets(
    "cpp", {
        s({
            trig = "if",
            dscr = "If statement with choice of initializer",
            name = "if statement",
        }, fmt([[
        if (<cond>)
        {
            <body>
        }
        ]], {
            cond = c(1, {
                sn(1, fmt("<init>; <condition>", {
                    init = i(1, "{init}"),
                    condition = i(2, "{condition}"),
                })),
                sn(2, fmt("<condition>", {
                    condition = i(1, "{condition}"),
                })),
            }),
            body = d(2, selected_or_body)
        })),
        s({
            trig = "for",
            dscr = "Either classic or ranged based for loop",
            name = "Choice of for loop",
        }, fmt([[
            for (<cond>)
            {
                <body>
            }
            ]], {
                cond = c(1, {
                    sn(1, fmt("<init> : <range>",{
                        init = i(1, "{init}"),
                        range = i(2, "{range}")
                    })),
                    sn(2, fmt("<init>; <cond>; <post>", {
                        init = i(1, "{init}"),
                        cond = i(2, "{cond}"),
                        post = i(3, "{post}"),
                    })),
                }),
                body = d(2, selected_or_body)
            })
        )
    }
)
