module mmfood.org/barista-bar

go 1.16

require (
	barista.run v0.0.0-20210325193727-a24225136e5b
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/martinlindhe/unit v0.0.0-20210313160520-19b60e03648d
)
