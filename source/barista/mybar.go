// Copyright 2018 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// simple demonstrates a simpler i3bar built using barista.
// Serves as a good starting point for building custom bars.
package main

import (
	"fmt"
	"os/exec"
	"os/user"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"barista.run"
	"barista.run/bar"
	"barista.run/base/click"
	"barista.run/base/watchers/netlink"
	"barista.run/colors"
	"barista.run/format"
	"barista.run/group"
	"barista.run/group/collapsing"
	"barista.run/modules/battery"
	"barista.run/modules/clock"
	"barista.run/modules/cputemp"
	"barista.run/modules/diskspace"
	"barista.run/modules/media"
	"barista.run/modules/meminfo"
	"barista.run/modules/netinfo"
	"barista.run/modules/netspeed"
	"barista.run/modules/shell"
	"barista.run/modules/static"
	"barista.run/modules/sysinfo"
	"barista.run/modules/volume"
	"barista.run/modules/volume/alsa"
	"barista.run/modules/wlan"
	"barista.run/outputs"
	"barista.run/pango"
	"barista.run/pango/icons/fontawesome"
	"barista.run/pango/icons/material"
	"barista.run/pango/icons/mdi"
	"barista.run/pango/icons/typicons"

	"github.com/martinlindhe/unit"
)

var spacer = pango.Text(" ").XXSmall()

func truncate(in string, l int) string {
	if len([]rune(in)) <= l {
		return in
	}
	return string([]rune(in)[:l-1]) + "⋯"
}

func hms(d time.Duration) (h int, m int, s int) {
	h = int(d.Hours())
	m = int(d.Minutes()) % 60
	s = int(d.Seconds()) % 60
	return
}

func formatMediaTime(d time.Duration) string {
	h, m, s := hms(d)
	if h > 0 {
		return fmt.Sprintf("%d:%02d:%02d", h, m, s)
	}
	return fmt.Sprintf("%d:%02d", m, s)
}

func mediaFormatFunc(m media.Info) bar.Output {
	if m.PlaybackStatus == media.Stopped || m.PlaybackStatus == media.Disconnected {
		return nil
	}
	artist := truncate(m.Artist, 20)
	title := truncate(m.Title, 40-len(artist))
	if len(title) < 20 {
		artist = truncate(m.Artist, 40-len(title))
	}
	iconAndPosition := pango.Icon("fa-music").Color(colors.Hex("#f70"))
	if m.PlaybackStatus == media.Playing {
		iconAndPosition.Append(
			spacer, pango.Textf("%s/%s",
				formatMediaTime(m.Position()),
				formatMediaTime(m.Length)),
		)
	}
	return outputs.Pango(iconAndPosition, spacer, title, " - ", artist)
}

var startTaskManager = click.RunLeft("alacritty", "-e", "gotop")
var volumeManager = click.Right(func() { exec.Command("pavucontrol").Run() })
var volumeMute = click.Left(func() { exec.Command("pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle").Run() })
var volumeClick = func(evt bar.Event) {
	switch evt.Button {
	case bar.ButtonLeft:
		volumeMute(evt)
	case bar.ButtonRight:
		volumeManager(evt)
	}
}

func vpnConnect(evt bar.Event) {
	exec.Command("expressvpn", "connect").Run()
}

func vpnDisconnect(evt bar.Event) {
	exec.Command("expressvpn", "disconnect").Run()
}

func home(path string) string {
	usr, err := user.Current()
	if err != nil {
		panic(err)
	}
	return filepath.Join(usr.HomeDir, path)
}

var dateToggle = false

func showdate(now time.Time) bar.Output {
	return outputs.Pango(
		pango.Icon("fa-calendar-day").Color(colors.Scheme("dim-icon")),
		spacer,
		now.Format("Mon Jan 2 "),
	).OnClick(click.LeftE(func(bar.Event) { dateToggle = false })).
		Color(colors.Scheme("text"))
}

func showtime(now time.Time) bar.Output {
	return outputs.Pango(
		pango.Icon("fa-clock").Color(colors.Scheme("dim-icon")),
		spacer,
		now.Format("15:04"),
	).OnClick(click.LeftE(func(bar.Event) { dateToggle = true })).
		Color(colors.Scheme("text"))
}

func batteryIcon(i battery.Info) string {
	switch i.Status {
	case battery.Charging:
		return "fa-bolt"
	case battery.Discharging:
		tenth := i.RemainingPct() / 10
		switch {
		case tenth < 1:
			return "fa-battery-empty"
		case tenth < 3:
			return "fa-battery-quarter"
		case tenth <= 5:
			return "fa-battery-half"
		case tenth < 8:
			return "fa-battery-three-quarters"
		default:
			return "fa-battery-full"
		}
	default:
		return "fa-plug"
	}
}

func pangoIcon(name string) *pango.Node {
	return pango.Icon(name).Color(colors.Scheme("dim-icon"))
}

func main() {
	material.Load(home("Github/material-design-icons"))
	mdi.Load(home("Github/MaterialDesign-Webfont"))
	typicons.Load(home("Github/typicons.font"))
	fontawesome.Load(home("GitHub/Font-Awesome"))

	colors.LoadFromMap(map[string]string{
		"good":       "#a3be8c",
		"bad":        "#bf616a",
		"degraded":   "#ebcb8b",
		"background": "#4c566a",
		"dim-icon":   "#4c566a",
		"text":       "#d8dee9",
	})

	localtime := clock.Local().
		Output(100*time.Millisecond, func(now time.Time) bar.Output {
			if dateToggle {
				return showdate(now)
			} else {
				return showtime(now)
			}
		})

	vpn := netinfo.Prefix("tun").Output(func(s netinfo.State) bar.Output {
		if len(s.IPs) > 0 {
			out := outputs.Pango(pango.Icon("fa-lock").Color(colors.Scheme("good")))
			out.OnClick(vpnDisconnect)
			return out
		}
		out := outputs.Pango(pangoIcon("fa-lock-open").Color(colors.Scheme("bad")))
		out.OnClick(vpnConnect)
		return out
	})

	buildBattOutput := func(i battery.Info, disp *pango.Node) *bar.Segment {
		iconName := batteryIcon(i)
		out := outputs.Pango(pango.Icon(iconName), disp)
		switch {
		case i.RemainingPct() <= 5:
			out.Urgent(true)
		case i.RemainingPct() <= 15:
			out.Color(colors.Scheme("bad"))
		case i.RemainingPct() <= 25:
			out.Color(colors.Scheme("degraded"))
		}
		return out
	}
	var showBattPct, showBattTime func(battery.Info) bar.Output

	batt := battery.All()
	showBattPct = func(i battery.Info) bar.Output {
		out := buildBattOutput(i, pango.Textf(" %d%%", i.RemainingPct()))
		if out == nil {
			return nil
		}
		return out.OnClick(click.Left(func() {
			batt.Output(showBattTime)
		}))
	}
	showBattTime = func(i battery.Info) bar.Output {
		rem := i.RemainingTime()
		out := buildBattOutput(i, pango.Textf(
			"%d:%02d", int(rem.Hours()), int(rem.Minutes())%60))
		if out == nil {
			return nil
		}
		return out.OnClick(click.Left(func() {
			batt.Output(showBattPct)
		}))
	}
	batt.Output(showBattPct)

	vol := volume.New(alsa.DefaultMixer()).Output(func(v volume.Volume) bar.Output {
		if v.Mute {
			return outputs.
				Pango(pango.Icon("fa-volume-mute"), spacer, "MUT").
				Color(colors.Scheme("degraded"))
		}
		iconName := "off"
		pct := v.Pct()
		if pct > 66 {
			iconName = "up"
		} else if pct > 33 {
			iconName = "down"
		}
		out := outputs.Pango(
			pango.Icon("fa-volume-"+iconName),
			spacer,
			pango.Textf("%2d%%", pct),
		)
		out.OnClick(volumeClick)
		return out
	})

	loadAvg := sysinfo.New().Output(func(s sysinfo.Info) bar.Output {
		perc := s.Loads[0] * 100 / float64(runtime.NumCPU())
		out := outputs.
			Pango(pango.Icon("fa-microchip"), spacer, fmt.Sprintf("%0.2f %d%%", s.Loads[0], int(perc)))
		// out := outputs.Textf("%0.2f %0.2f", s.Loads[0], s.Loads[2])
		// Load averages are unusually high for a few minutes after boot.
		if s.Uptime < 10*time.Minute {
			// so don't add colours until 10 minutes after system start.
			return out
		}
		switch {
		case s.Loads[0] > 128, s.Loads[2] > 64:
			out.Urgent(true)
		case s.Loads[0] > 64, s.Loads[2] > 32:
			out.Color(colors.Scheme("bad"))
		case s.Loads[0] > 32, s.Loads[2] > 16:
			out.Color(colors.Scheme("degraded"))
		}
		out.OnClick(startTaskManager)
		return out
	})

	freeMem := meminfo.New().Output(func(m meminfo.Info) bar.Output {
		out := outputs.Pango(pango.Icon("fa-memory"), spacer, format.IBytesize(m.Available()))
		freeGigs := m.Available().Gigabytes()
		switch {
		case freeGigs < 0.5:
			out.Urgent(true)
		case freeGigs < 1:
			out.Color(colors.Scheme("bad"))
		case freeGigs < 2:
			out.Color(colors.Scheme("degraded"))
		}
		out.OnClick(startTaskManager)
		return out
	})

	temp := cputemp.New().
		RefreshInterval(2 * time.Second).
		Output(func(temp unit.Temperature) bar.Output {
			out := outputs.Pango(
				pango.Icon("fa-temperature-high"), spacer,
				pango.Textf("%2d℃", int(temp.Celsius())),
			)
			switch {
			case temp.Celsius() > 90:
				out.Urgent(true)
			case temp.Celsius() > 70:
				out.Color(colors.Scheme("bad"))
			case temp.Celsius() > 60:
				out.Color(colors.Scheme("degraded"))
			}
			return out
		})
	cpuGrp, _ := collapsing.Group(temp, loadAvg)
	cpuPanel := group.Simple(static.New(outputs.Pango(pango.Text("[CPU]"))), cpuGrp)

	sub := netlink.Any()
	iface := sub.Get().Name
	sub.Unsubscribe()
	net := netspeed.New(iface).
		RefreshInterval(2 * time.Second).
		Output(func(s netspeed.Speeds) bar.Output {
			return outputs.Pango(
				pango.Icon("fa-long-arrow-alt-up"), spacer, pango.Textf("%7s", format.Byterate(s.Tx)),
				pango.Text(" ").Small(),
				pango.Icon("fa-long-arrow-alt-down"), spacer, pango.Textf("%7s", format.Byterate(s.Rx)),
			)
		})

	wlan := wlan.Any().Output(func(i wlan.Info) bar.Output {
		var text string
		switch {
		case !i.Enabled():
			return nil
		case i.Connecting():
			text = "W: ..."
		case !i.Connected():
			text = "W: down"
		case len(i.IPs) < 1:
			text = fmt.Sprintf("%s (...)", i.SSID)
		default:
			text = fmt.Sprintf("%s (%s)[%.0f]", i.SSID, i.IPs[0], i.Frequency.Gigahertz())
		}
		return outputs.Pango(pango.Icon("fa-wifi"), spacer, text)
	})
	netGrp, _ := collapsing.Group(wlan, net)
	netPanel := group.Simple(static.New(outputs.Pango(pango.Text("[Net]"))), netGrp)

	homeDisk := diskspace.New("/home").Output(func(i diskspace.Info) bar.Output {
		return outputs.Textf("Home %.fGB", i.Available.Gibibytes())
	})

	rootDisk := diskspace.New("/").Output(func(i diskspace.Info) bar.Output {
		return outputs.Textf("Root %.fGB", i.Available.Gibibytes())
	})
	diskGrp, _ := collapsing.Group(rootDisk, homeDisk)
	diskPanel := group.Simple(static.New(outputs.Pango(pango.Text("[Disk]"))), diskGrp)

	backlight := shell.New("xbacklight", "-get").
		Every(100 * time.Millisecond).
		Output(func(count string) bar.Output {
			text := intStr(count)
			return outputs.Pango(pango.Icon("fa-adjust"), spacer, text)
		})

	panic(barista.Run(
		diskPanel,
		netPanel,
		cpuPanel,
		freeMem,
		vol,
		batt,
		backlight,
		vpn,
		localtime,
	))
}

func intStr(s string) string {
	i := strings.Index(s, ".")
	if i == -1 {
		return "err"
	}
	val, err := strconv.ParseInt(s[:i], 10, 32)
	if err != nil {
		return "err"
	}
	return fmt.Sprintf("%d%%", val)
}
