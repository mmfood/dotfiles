#!/bin/python3

import sys

if __name__ == "__main__":
    arglist = []

    if (len(sys.argv) > 1):
        arglist = sys.argv[1:]
    elif (len(sys.stdin) > 0):
        arglist = sys.stdin

    number = float(arglist[0])
    precision = 0

    if len(arglist) > 1:
        precision = int(arglist[1])

    print(round(number, precision))
