#!/bin/bash

# Configures the keyboard layouts for laptop keyboard, happy hacking keyboard and
# steelseries keyboard, using the files in ~/.config/xkb

sleep 1

logfile="/home/pabo/tmp/keyboard-setup-ok"
printf "Time: %s\n" "$(date +%H:%M:%S)" > "$logfile"

# Verify correct env
printf "Display: $DISPLAY \nXauthority: $XAUTHORITY \nHome: $HOME \n" >> "$logfile"

# Thinkpad
thinkpad="$(xinput list | awk '/AT Translated Set 2 keyboard/ {print substr($7,4)}')"
if [[ -n $thinkpad ]];then
    xkbcomp -I"$HOME"/.config/xkb -i "$thinkpad" "$HOME"/.config/xkb/pabo.xkb "$DISPLAY" >> "$logfile" 2>&1
    printf "thinkpad id: $thinkpad\n" >> "$logfile"
else
    printf "Did not set thinkpad\n" >> "$logfile"
fi

# HHKB
# hhkb="$(xinput list | awk '/HHKB/ {print substr($4,4)}')"
hhkb="$(xinput list | awk '/Limited HHKB-Hybrid \s*id/ {print substr($5,4)}')"
if [[ -n $hhkb ]];then
    xkbcomp -I"$HOME"/.config/xkb -i "$hhkb" "$HOME"/.config/xkb/hhkb.xkb "$DISPLAY" >> "$logfile" 2>&1
    printf "HHKB id: $hhkb\n" >> "$logfile"
else
    printf "Did not set hhkb\n" >> "$logfile"
fi

# SteelSeries 6Gv2
ss="$(xinput list | awk '/SteelS.*DATA\s+id/ {print substr($4,4)}')"
if [[ -n $ss ]];then
    xkbcomp -I"$HOME"/.config/xkb -i "$ss" "$HOME"/.config/xkb/pabo.xkb "$DISPLAY" >> "$logfile" 2>&1
    printf "Steelseries id: $ss\n" >> "$logfile"
else
    printf "Did not set steelseries!!!\n" >> "$logfile"
fi

