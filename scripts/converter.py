#!/bin/python3

import binascii
import re
from math import log2


def dectobin(dec_val):
    """Return binary representation of dec_val."""
    bin_str = str(bin(int(dec_val)))
    return _format_bin(bin_str.replace("0b", ""))


def dectohex(dec_str):
    """Return hexadecimal value of dec_str."""
    hex_str = str(hex(int(dec_str)))
    return hex_str.replace("0x", "").upper()


def hextodec(hex_str):
    """Return decimal value of hex_str."""
    dec_string = str(int(hex_str, 16))
    return dec_string.replace("0x", "")


def hextobin(hex_str):
    """Return the binary representaion of hex_str."""
    scale = 16
    num_of_bits = len(hex_str) * int(log2(scale))
    bin_str = bin(int(hex_str, scale))[2:].zfill(num_of_bits)
    return _format_bin(bin_str)


def bintodec(bin_str):
    """Return the decimal value represented by bin_str.

    bin_str can contain spaces.
    """
    dec_val = int(bin_str.replace(" ", ""), 2)
    return dec_val


def bintohex(bin_str):
    """Return hex value of bin_str.

    bin_str can contain spaces.
    """
    hex_val = hex(int(bin_str.replace(" ", ""), 2))
    return hex_val.replace("0x", "").upper()


def _format_bin(bin_str):
    """Separate bin_str into groups of four.

    Group with less than 4 digits will be filled with leading zeros:
    input    -> output
    ---------------------
    11110000 -> 1111 0000
    10000    -> 0001 0000
    """
    str_len = len(bin_str)
    remainder = str_len % 4
    factor = int(str_len / 4)

    # Fill string with zeros BEFORE inserting spaces.
    if remainder:
        bin_str = bin_str.zfill((factor + 1) * 4)

    return re.sub(r'((?:(.)){4})(?!$)', r'\1 ', bin_str)


if __name__ == "__main__":

    # A mapping dictionary in the form  menu-value: function-name
    opts = {
            '1': bintodec,
            '2': bintohex,
            '3': hextobin,
            '4': hextodec,
            '5': dectobin,
            '6': dectohex,
            }


    while True:
        print("""\
        --------------------------
        | Menu:                  |
        --------------------------
            1) binary to dec
            2) binary to hex
            3) hexadec to binary
            4) hexadec to dec
            5) decimal to binary
            6) decimal to hexadec

            0) exit
        --------------------------
        """)

        choice = input("\tchoice: ")
        if choice == '0':
            exit()

        inval = input(f"""
        ---------------------
        {opts[choice].__name__}
        ---------------------
        Enter value to convert:
        """)
        result = opts[choice](inval)

        print(f"""
        {opts[choice].__name__}({inval.upper()})
        -----------------------------------------
        Result: {result}
        """)

