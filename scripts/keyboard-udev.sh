#!/bin/bash

# Use with udev rule:
# SUBSYSTEM=="input", SUBSYSTEMS=="usb", ACTION=="add", \
#      RUN+="/bin/su pabo -c /home/pabo/.local/bin/keyboard-udev.sh"

lock="/tmp/__pabo_kblock"
if [[ -f "$lock" ]]; then
    exit 0
fi
trap "{ sleep 1; rm -rf ${lock}; }" EXIT
touch "$lock"

# Easiest to set these here
DISPLAY=":0"
XAUTHORITY="$HOME/.Xauthority"
export DISPLAY XAUTHORITY

# This script should wait a sec, and then set xkblayouts using xkbcomp
~/.local/bin/keyboard.sh &
