#!/bin/bash

# The purpose of this script is to suspend the machine
# if the average capacity of the connected batteries is
# below the threshold.

suspend_cmd="/usr/bin/systemctl suspend"
devpath="/sys/class/power_supply"
let suspend_threshold=15

# Debounce calls from udev by using a lockfile and sleeping before script ends.
lockfile="/tmp/battery-lock"
if [[ -f "$lockfile" ]]; then
	exit 0
fi
trap "{ rm -rf ${lockfile}; }" EXIT
touch "$lockfile"

printf '[[ %s ]] running lower power handler:\n' "$(date +"%G-%m-%d %H:%M")" >> /home/pabo/batlog

charging=false 
let sum=0
let count=0
for battery in "$devpath"/BAT*; do 
    ((count++))
    let capacity="$(cat $battery/capacity)"

    (( sum += capacity ))
    printf "$battery: ${capacity}%%\n" >> /home/pabo/batlog

    state="$(cat $battery/status)"
    if [[ "$state" == "Charging" ]] ;then
        charging=true
    fi
done

let avg=0
(( avg = sum / count ))
printf "Average: ${avg}%%\n" >> /home/pabo/batlog

if [ $charging = false ]; then
    printf "NOT charging!\n" >> /home/pabo/batlog
    if (( avg <= suspend_threshold )); then
	printf "Will suspend\n" >> /home/pabo/batlog
       	$suspend_cmd
    else
	printf "Will NOT suspend\n" >> /home/pabo/batlog
    fi
else
    printf "Charging!\n" >> /home/pabo/batlog
fi
printf "\n" >> /home/pabo/batlog
# Debounce
sleep 3
