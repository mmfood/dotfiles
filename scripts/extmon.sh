#!/bin/bash

# A script to configure multiple monitors

# Give xorg time to notice the change
sleep 2

laptop="eDP1"
externals=(
    "DP1"
    "DP2"
    "HDMI1"
    "HDMI2"
)

for ext in ${externals[@]}; do
    if xrandr | grep -q "^${ext} connected"; then
        xrandr --output "${ext}" --left-of "${laptop}" --auto
    else
        xrandr --output "${ext}" --auto
    fi
done


